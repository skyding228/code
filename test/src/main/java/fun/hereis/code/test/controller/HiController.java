package fun.hereis.code.test.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author weichunhe
 * created at 2021/9/16
 */
@RestController
@RequestMapping("/api")
public class HiController {


    @Value("${greet:未取到}")
    private String greet;

    @GetMapping("/greet/hello")
    public String hello(String name){
        return greet+ " hello "+name;
    }

    public void hi() {
        System.out.println("hello world");
    }
}
