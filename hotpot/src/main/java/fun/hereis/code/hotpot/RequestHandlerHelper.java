package fun.hereis.code.hotpot;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodIntrospector;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author weichunhe
 * created at 2021/9/30
 */
@Component
public class RequestHandlerHelper {

    private static AntPathMatcher antPathMatcher = new AntPathMatcher();
    private static Method registerHandlerMethodMethod;

    static {
        Method[] methods = ReflectionUtils.getAllDeclaredMethods(RequestMappingHandlerMapping.class);
        for (Method method : methods) {
            if("registerHandlerMethod".equals(method.getName())){
                ReflectionUtils.makeAccessible(method);
                registerHandlerMethodMethod = method;
            }
        }
    }
    static class MyRequestMappingHandlerMapping extends RequestMappingHandlerMapping {
        @Override
        public RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType){
            return super.getMappingForMethod(method,handlerType);
        }

    }

    private static MyRequestMappingHandlerMapping myRequestMappingHandlerMapping = new MyRequestMappingHandlerMapping();

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;

    public RequestHandlerHelper() {

    }




    /**
     * 注冊新处理器
     * @param handler
     * @param method
     * @param mapping
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public void registerHandlerMethod(Object handler, Method method, RequestMappingInfo mapping) {
        try {
            registerHandlerMethodMethod.invoke(handlerMapping,handler,method,mapping);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("注册处理器异常"+e.getMessage());
        }
    }

    /**
     * 注冊处理器
     * @param handler 处理器对象
     * @param antPathPattern 允许注册的路径蚂蚁表达式
     */
    public void registerHandlerMethod(final Object handler,String antPathPattern) {
        Class<?> handlerType = handler.getClass();
        final Class<?> userType = ClassUtils.getUserClass(handlerType);

        Map<Method, RequestMappingInfo> methods = MethodIntrospector.selectMethods(userType,
                new MethodIntrospector.MetadataLookup<RequestMappingInfo>() {
                    @Override
                    public RequestMappingInfo inspect(Method method) {
                        try {
                            return myRequestMappingHandlerMapping.getMappingForMethod(method, userType);
                        }
                        catch (Throwable ex) {
                            throw new IllegalStateException("Invalid mapping on handler class [" +
                                    userType.getName() + "]: " + method, ex);
                        }
                    }
                });

        for (Map.Entry<Method, RequestMappingInfo> entry : methods.entrySet()) {
            Method invocableMethod = AopUtils.selectInvocableMethod(entry.getKey(), userType);
            RequestMappingInfo mapping = entry.getValue();
            if(allowToRegister(antPathPattern,mapping)){
                handlerMapping.unregisterMapping(mapping);
                registerHandlerMethod(handler, invocableMethod, mapping);
            }else {
                //TODO
            }
        }
    }

    /**
     * 查看controller里面的路径是否允许注册
     * @param antPathPattern 路径表达式
     * @param info
     * @return
     */
    private boolean allowToRegister(String antPathPattern,RequestMappingInfo info){
        for (String s : info.getPatternsCondition().getPatterns()) {
            if(!antPathMatcher.match(antPathPattern,s)){
                return false;
            }
        }
        return true;
    }
}
