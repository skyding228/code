package fun.hereis.code.hotpot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;

/**
 * @author weichunhe
 * created at 2021/9/23
 */
@RestController
public class HotpotController {

    @Autowired
    private ConfigurableWebApplicationContext applicationContext;

    @Autowired
    private RequestHandlerHelper requestHandlerHelper;


    @GetMapping("/register")
    public String register(){
        String[] jars = new String[]{"E:\\intelljws\\code\\test\\target\\test-3.0.RLS.jar"};
        String basePackage = "fun.hereis.code.test.controller";
        String antPath = "/api/greet/**";
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        URLClassLoader classLoader = null;
        try {
            URL[] urls = new URL[jars.length];
            for (int i = 0; i < jars.length; i++) {
                urls[i] = new File(jars[i]).toURI().toURL();
            }
            classLoader = new URLClassLoader(urls,context.getClass().getClassLoader());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String resourcePath = basePackage.replaceAll("\\.","/")+"/hotpot.properties";
        synchronized (HotpotEnvironmentParams.class){
            HotpotEnvironmentParams.classLoader = classLoader;
            context.setEnvironment(new HotpotEnvironment());
        }

        context.setClassLoader(classLoader);
        context.scan(basePackage);
        context.refresh();
        Map<String,Object> beansMap = context.getBeansWithAnnotation(Controller.class);
        Map<String,RequestMappingHandlerMapping> handlerMappingMap = applicationContext.getBeansOfType(RequestMappingHandlerMapping.class);

        beansMap.forEach((k,v)->{
            requestHandlerHelper.registerHandlerMethod(v,antPath);
        });

        return "success";
    }

    @GetMapping("/hi")
    public String hello(String name){
        return "hi "+name;
    }
}
