package fun.hereis.code.hotpot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author weichunhe
 * created at 2021/9/9
 */
@SpringBootApplication()
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }
}
