package fun.hereis.code.spring;

import fun.hereis.code.spring.ClasspathPropertyUtil;
import io.lettuce.core.resource.DnsResolver;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RedisDnsResolver implements DnsResolver {

    public static final DnsResolver DNS_RESOLVER = new RedisDnsResolver();

    private String dnsMapKey = "redis.dns.map";

    private Map<String, String> dnsMap = new HashMap<>();

    public RedisDnsResolver() {
        Properties redisProps = ClasspathPropertyUtil.load("application.properties");
        String data = redisProps.getProperty(dnsMapKey);
        if (StringUtils.isEmpty(data)) {
            return;
        }
        String[] entrys = data.split(",");
        for (String entry : entrys) {
            String[] items = entry.split(":");
            if (items.length == 2) {
                dnsMap.put(items[0], items[1]);
            }
        }
    }

    @Override
    public InetAddress[] resolve(String host) throws UnknownHostException {
        String target = dnsMap.get(host);
        if (StringUtils.isEmpty(target)) {
            return DnsResolver.jvmDefault().resolve(host);
        }
        InetAddress[] inetAddresses = InetAddress.getAllByName(target);
        String ips = Stream.of(inetAddresses).map(InetAddress::getHostAddress).collect(Collectors.joining(","));
        System.out.println("redis解析 ----------------- " + host + "->" + ips);
        return inetAddresses;
    }
}
