package fun.hereis.code.spring;

import io.lettuce.core.cluster.api.async.RedisClusterAsyncCommands;
import io.lettuce.core.cluster.api.sync.RedisClusterCommands;

import java.util.Properties;

/**
 * redis客户端
 * 静态方法默认读取 redis.properties 中的redis.nodes 和 redis.password 进行创建集群连接
 *
 * @author weichunhe
 * created at 19-8-17
 */
public class Lettuce {

    static LettuceClient client;


    private static void init() {
        if (client != null) {
            return;
        }
        synchronized (Lettuce.class) {
            if (client != null) {
                return;
            }
            Properties redisProps = ClasspathPropertyUtil.load("redis.properties");
            Properties appProps = ClasspathPropertyUtil.load("application.properties");
            String address = redisProps.getProperty("redis.nodes", appProps.getProperty("redis.nodes", "localhost:6379"));
            String password = redisProps.getProperty("redis.password", appProps.getProperty("redis.password"));
            client = LettuceClient.newInstance(address, password);
        }

    }

    private Lettuce() {
    }


    /**
     * @return 同步命令
     */
    public static RedisClusterCommands<String, String> sync() {
        init();
        return client.sync();
    }

    /**
     * @return 异步命令
     */
    public static RedisClusterAsyncCommands<String, String> async() {
        init();
        return client.async();
    }

}
