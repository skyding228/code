package fun.hereis.code.spring;

import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.ClusterTopologyRefreshOptions;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.async.RedisClusterAsyncCommands;
import io.lettuce.core.cluster.api.sync.RedisClusterCommands;
import io.lettuce.core.codec.ByteArrayCodec;
import io.lettuce.core.internal.HostAndPort;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.resource.DefaultClientResources;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * redis 客户端，基于lettuce依赖包
 *
 * @author weichunhe
 * created at 19-8-17
 */
public class LettuceClient {

    boolean isCluster = false;

    StatefulRedisClusterConnection<String, String> clusterConnection;

    StatefulRedisClusterConnection<byte[], byte[]> byteConnection;
    StatefulRedisConnection<String, String> connection;

    /**
     * 创建实例
     *
     * @param addr     多个地址用,分割, 只有1个地址时会被认为单节点，集群地址请至少传递两个地址
     * @param password 集群密码，没有可传null
     * @return
     */
    public static LettuceClient newInstance(String addr, String password) {
        return new LettuceClient().init(addr, password);
    }

    /**
     * 根据链接地址创建客户端passwd@host:ip,host:ip
     *
     * @param uri
     * @return
     */
    public static LettuceClient newInstance(String uri) {
        int index = uri.lastIndexOf("@");
        return newInstance(uri.substring(index + 1), uri.substring(0, index));
    }

    private LettuceClient init(String addr, String password) {
        Set<HostAndPort> nodes = getNodes(addr);
        if (nodes.size() == 1) {
            //get the only one host and port
            HostAndPort hostAndPort = nodes.toArray(new HostAndPort[1])[0];
            RedisURI uri = RedisURI.create(hostAndPort.getHostText(), hostAndPort.getPort());
            if (StringUtils.isNotEmpty(password)) {
                uri.setPassword(password);
            }
            io.lettuce.core.RedisClient client = io.lettuce.core.RedisClient.create(uri);
            connection = client.connect();
        } else {
            isCluster = true;
            RedisClusterClient clusterClient = getRedisClusterClient(password, nodes);
            clusterConnection = clusterClient.connect();
            byteConnection = clusterClient.connect(ByteArrayCodec.INSTANCE);
        }
        heartbeat();
        return this;
    }

    private void heartbeat() {
        LettuceClient that = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        String key = UUID.randomUUID().toString() + "-heartbeat";
                        that.sync().setex(key, 5, System.currentTimeMillis() + "");
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    static RedisClusterClient getRedisClusterClient(String password, Set<HostAndPort> nodes) {
        List<RedisURI> uris = new ArrayList<>();
        nodes.forEach(n -> {
            RedisURI uri = RedisURI.create(n.getHostText(), n.getPort());
            if (StringUtils.isNotEmpty(password)) {
                uri.setPassword(password);
            }
            uris.add(uri);
        });
        ClientResources clientResources = DefaultClientResources.builder().dnsResolver(RedisDnsResolver.DNS_RESOLVER).build();
        RedisClusterClient clusterClient = RedisClusterClient.create(clientResources, uris);
        ClusterTopologyRefreshOptions topologyRefreshOptions = ClusterTopologyRefreshOptions.builder()
//                    .enablePeriodicRefresh(true)
                .enableAllAdaptiveRefreshTriggers()
                .build();

        clusterClient.setOptions(ClusterClientOptions.builder()
                .topologyRefreshOptions(topologyRefreshOptions)
                .build());
        return clusterClient;
    }

    private LettuceClient() {
    }

    /**
     * @return 同步命令
     */
    public RedisClusterCommands<String, String> sync() {
        return isCluster ? clusterConnection.sync() : connection.sync();
    }

    /**
     * @return 同步命令
     */
    public RedisClusterCommands<byte[], byte[]> byteSync() {
        return byteConnection.sync();
    }

    /**
     * @return 异步命令
     */
    public RedisClusterAsyncCommands<String, String> async() {
        return isCluster ? clusterConnection.async() : connection.async();
    }

    /**
     * 获取配置节点
     *
     * @param addr 多个地址用,分割, 只有1个地址时会被认为单节点，集群地址请至少传递两个地址
     * @return
     */
    static Set<HostAndPort> getNodes(String addr) {
        String[] addresses = addr.replaceAll(" +", "").split(",");
        Set<HostAndPort> hostAndPorts = new HashSet<>();
        for (String address : addresses) {
            if (StringUtils.isNotEmpty(address)) {
                hostAndPorts.add(HostAndPort.parse(address));
            }
        }
        return hostAndPorts;
    }

}

