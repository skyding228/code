package fun.hereis.code.spring;

import java.util.HashMap;
import java.util.Map;

/**
 * redis 路由工具
 */
public class LettuceRouter {
    private static char[] indexChars = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static Map<Character, Integer> indexMap = new HashMap<>();

    static {
        for (int i = 0; i < indexChars.length; i++) {
            indexMap.put(indexChars[i], i);
        }
    }

    private LettuceRouter defaultRouter;

    private LettuceClient[] clients;

    /**
     * 根据字符串的最后一位进行路由客户端
     *
     * @return
     */
    public LettuceClient route(String routerChar) {
        return defaultRouter.getClient(routerChar);
    }


    /**
     * 根据连接创建客户端
     *
     * @param uris
     */
    public LettuceRouter(String uris) {
        String[] conns = uris.split(";");
        clients = new LettuceClient[conns.length];
        for (int i = 0; i < conns.length; i++) {
            clients[i] = LettuceClient.newInstance(conns[i]);
        }
        defaultRouter = this;
    }

    /**
     * 根据最后一个字符查找客户端
     *
     * @param routerChar
     * @return
     */
    public LettuceClient getClient(String routerChar) {
        char indexChar = routerChar.toLowerCase().charAt(routerChar.length() - 1);
        Integer index = indexMap.getOrDefault(indexChar, 0);
        return clients[index % clients.length];
    }

}
